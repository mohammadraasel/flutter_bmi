import 'package:flutter/material.dart';

class IconContent extends StatelessWidget {
  final IconData icon;
  final String label;

  IconContent({this.icon, this.label});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Icon(
          icon,
          size: 70.0,
        ),
        SizedBox(
          height: 13,
        ),
        Text(
          label,
          style: TextStyle(
            fontSize: 16.0,
            color: Color(0xFF8D8E98),
          ),
        ),
      ],
    );
  }
}