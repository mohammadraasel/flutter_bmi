import 'package:flutter/material.dart';

const kLabelTextStyle = TextStyle(
  fontSize: 15.0,
  color: Color(0xFF8D8E98),
);

const kNumberStyle = TextStyle(
  fontSize: 40,
  fontWeight: FontWeight.w900,
);

const kActiveCardColor = Color(0xFF1D1E33);
const kInactiveCardColor = Color(0xFF111328);
